section .text

%define sys_exit 60
%define read 0
%define write 1
%define stdin 0
%define stdout 1
%define new_line 0xA
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
%define space 0x20
%define tab 0x9

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, sys_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
      xor rax, rax
      .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
      .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov     rdx, rax
    mov     rsi, rdi
    mov     rax, 1
    mov     rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, new_line

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, write
    mov rsi, rsp
    mov rdx, 1
   mov rdi, stdout
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .abovezero
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .abovezero:

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r8
    mov r8, rsp
    push 0
    mov rax, rdi
    mov rdi, 10

    .loop:
    mov rdx, 0
    div rdi
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    cmp rax, 0
    je .print
    jmp .loop

    .print:
    mov rdi, rsp
    push r8
    call print_string
    pop r8
    mov rsp, r8
    pop r8
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
xor r10, r10 ; счётчик
    xor r8, r8 ; регистр для символов первой строки
    xor r9, r9 ; регистр для символов второй строки

.loop:
    mov r8b, byte[rdi+r10]
    mov r9b, byte[rsi+r10]
    cmp r8b, r9b
    jne .error
    test r8b, r8b ; здесь на 0 можно проверить любой регистр, т.к. они равны
    je .end
    inc r10
    jmp .loop
.error:
    xor rax, rax
    ret
.end:
    mov rax, write
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
        push rdx
        push rsi
        push 0 ; пушим лбое число (в данном случае 0), чтобы сдвинуть вершину стрека
        mov rax, read ; sys_read 0
       mov rdi, stdin ; stdin descriptor
        mov rdx, 1 ; length = 1 sym
        mov rsi, rsp; rsi принимает адрес, прочитанный символ запишется на вершину стека
        syscall
        pop rax ; теперь прочитанный символ хранится в rax
        pop rdi
        pop rdx
        pop rsi
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    dec rsi ;уменьшили размер, чтобы дописать нультерминатор

        mov r8, rdi ; адрес начала буфера
        xor r9, r9 ; длина слова будет здесь
        mov r10, rsi; размер буфера

    .skip:                      ;пропуск начальных пробелов
        call read_char

        cmp al, 0
        je .end 	; конец потока - завершаем функцию
        cmp al, space
        je .skip
        cmp al, tab
        je .skip
        cmp al, new_line
        je .skip

    .loop:
        cmp al, 0
        je .end
        cmp al, space
        je .end
        cmp al, tab
        je .end
        cmp al, new_line
        je .end

        cmp r9, r10
        je .error

        mov byte[r8+r9], al
        inc r9

        call read_char
        jmp .loop
    .error:
        xor rax, rax
        xor rdx, rdx
        ret
    .end:
        mov rax, r8
        mov rdx, r9
        mov byte[r8+r9], 0
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
xor r8, r8
    xor rdx, rdx
    xor rax, rax
.loop:
    cmp byte [rdi + rdx], 0x30   ;
    jl .end			  ;	проверяем, является ли символ цифрой
    cmp byte [rdi + rdx], 0x39   ;
    jg .end			  ;
    mov r8b, byte [rdi + rdx]
    inc rdx
    imul rax, 10 ; "сдвиг" влево на 1 разряд
    sub r8b, 0x30 ; получаем из ASCII кода непосредственно цифру
    add rax, r8
    jmp .loop
.end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
cmp byte [rdi], '-'
    jne .abovezero
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .error
    inc rdx
    neg rax
    ret
.error:
    xor rax,rax
    ret
.abovezero:
    jmp parse_uint

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r10, r10
        cmp rdx, 0
        je .zerobuffer
        mov r10b, byte[rdi]
        mov byte[rsi], r10b
        dec rdx
        inc rdi
        inc rsi
        test r10, r10
        jne string_copy
        ret
        .zerobuffer:
        xor rax, rax
        ret
